const { SuperSocketServer, SuperSocketClient } = require('../lib');

const PORT = process.env['PORT'] || 5555;

let client, server;
let clientOpened;

beforeEach(() => {
  server = new SuperSocketServer({ ws: { port: PORT } });
  client = new SuperSocketClient(`ws://localhost:${PORT}`);
  clientOpened = new Promise(resolve => client.once('open', resolve));
});

afterEach(async done => {
  await clientOpened;
  client.close();
  await server.close();
  done();
});

test('client receives events', done => {
  server.on('connection', client => {
    client.emitEvent('test event', 42);
  });

  client.addHandler('test event', secretNum => {
    expect(secretNum).toBe(42);
    done();
  });
});

test('server receives events', done => {
  server.addHandler('test event', (client, secretNum) => {
    expect(secretNum).toBe(42);
    done();
  });

  client.on('open', () => {
    client.emitEvent('test event', 42);
  });
});

describe('client responds', () => {
  test('success', done => {
    server.on('connection', async remoteClient => {
      const response = await remoteClient.request('sum', { a: 40, b: 2 });
      expect(response).toBe(42);
      done();
    });

    client.addHandler('sum', ({ a, b }) => a + b);
  });

  test('failure', done => {
    server.on('connection', async remoteClient => {
      remoteClient.request('div', { a: 12.4, b: 0 }).catch(err => {
        expect(err).toBe('divByZero');
        done();
      });
    });

    client.addHandler('div', ({ a, b }) => {
      if (b === 0) {
        throw 'divByZero';
      } else {
        return a / b;
      }
    });
  });

  test('success promise', done => {
    server.on('connection', async remoteClient => {
      const response = await remoteClient.request('sum', { a: 40, b: 2 });
      expect(response).toBe(42);
      done();
    });

    client.addHandler('sum', ({ a, b }) => Promise.resolve(a + b));
  });

  test('failure promise', done => {
    server.on('connection', async remoteClient => {
      remoteClient.request('div', { a: 12.5, b: 0 }).catch(err => {
        expect(err).toBe('divByZero');
        done();
      });
    });

    client.addHandler('div', ({ a, b }) => {
      if (b === 0) {
        return Promise.reject('divByZero');
      } else {
        return Promise.resolve(a / b);
      }
    });
  });
});

describe('server responds', () => {
  test('success', done => {
    server.addHandler('sum', (client, { a, b }) => a + b);

    client.on('open', async () => {
      const response = await client.request('sum', { a: 40, b: 2 });
      expect(response).toBe(42);
      done();
    });
  });

  test('failure', done => {
    server.addHandler('arraySum', (client, nums) => {
      if (!nums.length) {
        throw 'arrayExpected';
      }

      return nums.reduce((a, b) => a + b, 0);
    });

    client.on('open', async () => {
      client.request('arraySum', { a: 40, b: 2 }).catch(err => {
        expect(err).toBe('arrayExpected');
        done();
      });
    });
  });

  test('success promise', done => {
    server.addHandler('sum', (client, { a, b }) => Promise.resolve(a + b));

    client.on('open', async () => {
      const response = await client.request('sum', { a: 40, b: 2 });
      expect(response).toBe(42);
      done();
    });
  });

  test('failure promise', done => {
    server.addHandler('arraySum', (client, nums) => {
      if (!nums.length) {
        return Promise.reject('arrayExpected');
      }

      return Promise.resolve(nums.reduce((a, b) => a + b, 0));
    });

    client.on('open', async () => {
      client.request('arraySum', { a: 40, b: 2 }).catch(err => {
        expect(err).toBe('arrayExpected');
        done();
      });
    });
  });
});