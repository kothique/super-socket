const msgpack = require('msgpack-lite');

const REQUEST = 0;
const RESPONSE_SUCCESS = 1;
const RESPONSE_FAILURE = 2;
const EVENT = 3;

class Message {
  constructor(options) {
    this._type = options.type;
    this.topic = options.topic;
    this.ref = options.ref;
    this.payload = options.payload;
  }

  isRequest() {
    return this._type === REQUEST;
  }

  isResponse() {
    return this._type === RESPONSE_FAILURE || this._type === RESPONSE_SUCCESS;
  }

  isSuccessfulResponse() {
    return this._type === RESPONSE_SUCCESS;
  }

  isFailedResponse() {
    return this._type === RESPONSE_FAILURE;
  }

  isEvent() {
    return this._type === EVENT;
  }

  serialize() {
    return msgpack.encode(this._toJSON());
  }

  respondSuccess(payload = null) {
    return new Message({
      type: RESPONSE_SUCCESS,
      ref: this.ref,
      payload
    });
  }

  respondFailure(payload = null) {
    return new Message({
      type: RESPONSE_FAILURE,
      ref: this.ref,
      payload
    });
  }

  _toJSON() {
    if (this._type === REQUEST) {
      return {
        t: REQUEST,
        s: this.topic,
        r: this.ref,
        ...this.payload && { p: this.payload }
      };
    } else if (this._type === RESPONSE_SUCCESS || this._type === RESPONSE_FAILURE) {
      return {
        t: this._type,
        r: this.ref,
        ...this.payload && { p: this.payload }
      };
    } else if (this._type === EVENT) {
      return {
        t: EVENT,
        s: this.topic,
        ...this.payload && { p: this.payload }
      };
    }
  }
};

module.exports.createRequest = function createRequest(topic, ref, payload = null) {
  return new Message({
    type: REQUEST,
    topic,
    ref,
    ...payload && { payload }
  });
}

module.exports.createEvent = function createEvent(topic, payload = null) {
  return new Message({
    type: EVENT,
    topic,
    ...payload && { payload }
  });
}

module.exports.deserialize = function deserialize(data) {
  const msg = msgpack.decode(data);

  return new Message({
    type: msg.t,
    topic: msg.s,
    ref: msg.r,
    payload: msg.p
  });
}