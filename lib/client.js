const EventEmitter = require('events');
const WebSocket = require('ws');
const { createRequest, createEvent, deserialize } = require('./message');

const defaultOptions = {
  timeout: 5000
};

class SuperSocketClient extends EventEmitter {
  /**
   * @param {string}  address
   * @param {?object} options
   * @param {?number} options.timeout
   * @param {?object} options.ws
   */
  constructor(address, options = defaultOptions) {
    super();

    this._options = options;

    this._handlers = {};
    this._newMsgRef = 0;
    this._refs = {};

    this._ws = new WebSocket(address, options.ws);

    this._ws.on('close', (code, reason) => {
      this.emit('close', code, reason);
    });
    this._ws.on('error', error => this.emit('error', error));
    this._ws.on('open', () => this.emit('open'));
    this._ws.on('ping', data => this.emit('ping', data));
    this._ws.on('pong', data => this.emit('pong', data));
    this._ws.on('upgrade', response => this.emit('upgrade', response));

    this._ws.on('message', async data => {
      let msg;

      try {
        msg = deserialize(data);
      } catch(error) {
        console.warn('Failed to parse incoming msgpack message:', error);
        return;
      }

      if (msg.isEvent()) {
        this._handleEvent(msg);
      } else if (msg.isRequest()) {
        this._handleRequest(msg);
      } else if (msg.isResponse()) {
        this._handleResponse(msg);
      }
    });
  }

  /**
   * @private
   * @param {string} name
   * @param {?object} options
   * @returns {any}
   */
  _getOption(name, options = {}) {
    return options[name] || this._options[name];
  }

  /**
   * @param {any} topic
   * @param {(payload: ?any) => void|any|Promise<any>} cb
   * @returns {void}
   */
  addHandler(topic, cb) {
    if (!(topic in this._handlers)) {
      this._handlers[topic] = [];
    }
    const callbacks = this._handlers[topic];
    callbacks.push(cb);
  }

  /**
   * @param {any} topic
   * @param {(payload: ?any) => void|any|Promise<any>} cb
   * @returns {boolean} - Was deleted (true) or was not found (false).
   */
  removeHandler(topic, cb) {
    if (!(topic in this._handlers)) {
      return false;
    }

    const callbacks = this._handlers[topic];
    const index = callbacks.indexOf(cb);
    if (index === -1) {
      return false;
    } else {
      callbacks.splice(index, 1);
      if (callbacks.length === 0) {
        delete this._handlers[topic];
      }
      return true;
    }
  }

  /**
   * @param {any} topic
   * @param {?any} payload
   * @param {?object} options
   * @throws {TimeoutError}
   * @returns {Promise<any>}
   */
  request(topic, payload = null, options = {}) {
    const ref = this._newMsgRef++;

    const promise = new Promise((resolve, reject) => {
      const intervalId = setTimeout(() => reject(new TimeoutError), this._getOption('timeout', options));

      this._refs[ref] = [
        payload => {
          clearInterval(intervalId);
          resolve(payload);
        },
        error => {
          clearInterval(intervalId);
          reject(error);
        }
      ];
    });

    this._ws.send(createRequest(topic, ref, payload).serialize());

    return promise;
  }

  /**
   * @param {any} topic
   * @param {?any} payload
   * @param {?object} options
   * @returns {void}
   */
  emitEvent(topic, payload = null, options = {}) {
    this._ws.send(createEvent(topic, payload).serialize());
  }

  /**
   * @param {?number} code
   * @param {?string} reason
   */
  close(code, reason) {
    this._ws.close(code, reason);
  }

  /**
   * @private
   * @param {Message} msg
   */
  _handleEvent(msg) {
    const handlers = this._handlers[msg.topic];
    if (handlers) {
      handlers.forEach(handler => handler(msg.payload));
    }
  }

  /**
   * @private
   * @param {Message} msg - Request message.
   * @returns {Promise<Message>}
   */
  async _getResponse(msg) {
    const handlers = this._handlers[msg.topic];
    if (!handlers) {
      throw new InvalidRequestError('unknown topic: ' + msg.topic);
    }

    let payload = null;
    let error = false;
    await Promise.all(handlers.map(async handler => {
      try {
        const result = await handler(msg.payload);
        if (result && !error) {
          payload = result;
        }
      } catch (err) {
        error = true;
        payload = err;
      }
    }));

    return msg[error ? 'respondFailure' : 'respondSuccess'](payload);
  }

  /**
   * @private
   * @param {Message} msg
   * @returns {Promise<void>}
  */
  async _handleRequest(msg) {
    try {
      const result = await this._getResponse(msg);
      this._ws.send(result.serialize());
    } catch (error) {
      console.warn('Failed to get response from server:', error);
      return;
    }
  }

  /**
   * @private
   * @param {Message} msg
   */
  _handleResponse(msg) {
    if (this._refs[msg.ref]) {
      const [resolve, reject] = this._refs[msg.ref];
      delete this._refs[msg.ref];

      if (msg.isSuccessfulResponse()) {
        resolve(msg.payload);
      } else {
        reject(msg.payload);
      }
    }
  }
};

module.exports.SuperSocketClient = SuperSocketClient;