const WebSocket = require('ws');
const EventEmitter = require('events');
const { RemoteClient } = require('./remote-client');

const defaultOptions = {
  timeout: 5000
};

/**
 * @event {RemoteClient} connection
 */
class SuperSocketServer extends EventEmitter {
  /**
   * @param {?object} options
   * @param {?number} options.timeout
   * @param {?object} options.ws
   */
  constructor(options = {}) {
    super();

    this._options = {
      ...defaultOptions,
      ...options
    };
    this._handlers = {};

    this._wss = new WebSocket.Server(this._options.ws);

    this._wss.on('connection', (ws, req) => {
      const client = new RemoteClient(
        ws,
        this._handleEvent.bind(this),
        this._handleRequest.bind(this),
        this._options
      );
      this.emit('connection', client, req);
    });

    this._wss.on('close', () => this.emit('close'));
    this._wss.on('error', error => this.emit('error', error));
    this._wss.on('headers', (headers, req) => this.emit('headers', headers, req));
    this._wss.on('listening', () => this.emit('listening'));
  }

  /**
   * @param {any} topic
   * @param {(client: RemoteClient, payload: ?any) => void|any|Promise<any>} cb
   * @returns {void}
   */
  addHandler(topic, cb) {
    if (!(topic in this._handlers)) {
      this._handlers[topic] = [];
    }
    const callbacks = this._handlers[topic];
    callbacks.push(cb);
  }

  /**
   * @param {any} topic
   * @param {(client: RemoteClient, payload: ?any) => void|any|Promise<any>} cb
   * @returns {boolean} - Was deleted (true) or was not found (false).
   */
  removeHandler(topic, cb) {
    if (!(topic in this._handlers)) {
      return false;
    }

    const callbacks = this._handlers[topic];
    const index = callbacks.indexOf(cb);
    if (index === -1) {
      return false;
    } else {
      callbacks.splice(index, 1);
      if (callbacks.length === 0) {
        delete this._handlers[topic];
      }
      return true;
    }
  }

  /**
   * @param {Client} client
   * @param {Message} msg
   */
  _handleEvent(client, msg) {
    const handlers = this._handlers[msg.topic];
    if (handlers) {
      handlers.forEach(handler => handler(client, msg.payload));
    }
  }

  /**
   * @param {Client} client
   * @param {Message} msg
   * @returns {Promise<Message>}
   */
  async _handleRequest(client, msg) {
    const handlers = this._handlers[msg.topic];
    if (!handlers) {
      throw new InvalidRequestError('unknown topic: ' + msg.topic);
    }

    let payload = null;
    let error = false;
    await Promise.all(handlers.map(async handler => {
      try {
        const result = await handler(client, msg.payload);
        if (result && !error) {
          payload = result;
        }
      } catch (err) {
        error = true;
        payload = err;
      }
    }));

    return msg[error ? 'respondFailure' : 'respondSuccess'](payload);
  }

  /**
   * @returns {Promise<void>}
   */
  close() {
    return new Promise(resolve => this._wss.close(() => resolve()));
  }
};

module.exports.SuperSocketServer = SuperSocketServer;