const EventEmitter = require('events');

const { TimeoutError } = require('../errors');
const { createRequest, createEvent, deserialize } = require('../message');

class RemoteClient extends EventEmitter {
  constructor(ws, handleEvent, handleRequest, options) {
    super();

    this._options = options;

    this._newMsgRef = 0;
    this._refs = {};

    this._ws = ws;

    this._ws.on('message', async data => {
      let msg;

      try {
        msg = deserialize(data);
      } catch (error) {
        console.warn('Failed to parse incoming msgpack message:', error);
        return;
      }

      if (msg.isEvent()) {
        handleEvent(this, msg);
      } else if (msg.isRequest()) {
        try {
          const result = await handleRequest(this, msg);
          this._ws.send(result.serialize());
        } catch (error) {
          console.warn('Failed to get response from server:', error);
          return;
        }
      } else if (msg.isResponse()) {
        if (this._refs[msg.ref]) {
          const [resolve, reject] = this._refs[msg.ref];
          delete this._refs[msg.ref];

          if (msg.isSuccessfulResponse()) {
            resolve(msg.payload);
          } else {
            reject(msg.payload);
          }
        }
      }
    });

    this._ws.on('close', (code, reason) => this.emit('close', code, reason));
  }

  /**
   * @private
   * @param {string} name
   * @param {?object} options
   * @returns {any}
   */
  _getOption(name, options = {}) {
    return options[name] || this._options[name];
  }

  /**
   * @param {any} topic
   * @param {?any} payload
   * @param {?object} options
   * @throws {TimeoutError}
   * @returns {Promise<any>}
   */
  request(topic, payload = null, options = {}) {
    const ref = this._newMsgRef++;

    const promise = new Promise((resolve, reject) => {
      const intervalId = setTimeout(() => reject(new TimeoutError), this._getOption('timeout', options));

      this._refs[ref] = [
        payload => {
          clearInterval(intervalId);
          resolve(payload);
        },
        error => {
          clearInterval(intervalId);
          reject(error);
        }
      ];
    });

    this._ws.send(createRequest(topic, ref, payload).serialize());

    return promise;
  }

  /**
   * @param {any} topic
   * @param {?any} payload
   * @param {?object} options
   * @returns {void}
   */
  emitEvent(topic, payload = null, options = {}) {
    this._ws.send(createEvent(topic, payload).serialize());
  }

  /**
   * @param {?number} code
   * @param {?string} reason
   */
  close(code, reason) {
    this._ws.close(code, reason);
  }
};

module.exports.RemoteClient = RemoteClient;