import * as EventEmitter from 'events';
import * as WebSocket from 'ws';
import * as http from 'http';

interface ServerOptions {
  timeout?: number;
}

interface ServerWsOptions {
  ws?: WebSocket.ServerOptions;
}

declare interface RemoteClient {
  on(event: 'close', cb: (code: number, reason: string) => void): this;
  addListener(event: 'close', cb: (code: number, reason: string) => void): this;
  removeListener(event: 'close', cb: (code: number, reason: string) => void): this;
  request(topic: any, payload?: any, options?: ServerOptions): void;
  emitEvent(topic: any, payload?: any, options?: ServerOptions): void;
  close(code?: number, reason?: string): void;
}

export declare class SuperSocketServer extends EventEmitter {
  constructor(options?: ServerOptions & ServerWsOptions);
  addHandler(topic: any, cb: (client: RemoteClient, payload?: any) => void|any|Promise<any>): void;
  removeHandler(topic: any, cb: (client: RemoteClient, payload?: any) => void|any|Promise<any>): void;
  close(): Promise<void>;

  on(event: 'connection', cb: (client: RemoteClient, request: http.IncomingMessage) => void): this;
  on(event: 'error', cb: (error: Error) => void): this;
  on(event: 'headers', cb: (headers: string[], request: http.IncomingMessage) => void): this;
  on(event: 'listening', cb: () => void): this;

  addListener(event: 'connection', cb: (client: RemoteClient, request: http.IncomingMessage) => void): this;
  addListener(event: 'error', cb: (error: Error) => void): this;
  addListener(event: 'headers', cb: (headers: string[], request: http.IncomingMessage) => void): this;
  addListener(event: 'listening', cb: () => void): this;

  removeListener(event: 'connection', cb: (client: RemoteClient, request: http.IncomingMessage) => void): this;
  removeListener(event: 'error', cb: (error: Error) => void): this;
  removeListener(event: 'headers', cb: (headers: string[], request: http.IncomingMessage) => void): this;
  removeListener(event: 'listening', cb: () => void): this;
}

interface ClientOptions {
  timeout?: number;
}

interface ClientWsOptions {
  ws?: WebSocket.ClientOptions;
}

export declare class SuperSocketClient extends EventEmitter {
  constructor(address: string, options?: ClientOptions & ClientWsOptions);
  addHandler(topic: any, cb: (payload?: any) => void|any|Promise<any>): void;
  removeHandler(topic: any, cb: (payload?: any) => void|any|Promise<any>): void;
  request(topic: any, payload?: any, options?: ClientOptions): void;
  emitEvent(topic: any, payload?: any, options?: ClientOptions): void;
  close(code?: number, reason?: string): void;

  on(event: 'close', listener: (code: number, reason: string) => void): this;
  on(event: 'error', listener: (err: Error) => void): this;
  on(event: 'upgrade', listener: (request: http.IncomingMessage) => void): this;
  on(event: 'open' , listener: () => void): this;
  on(event: 'ping' | 'pong', listener: (data: Buffer) => void): this;

  addListener(event: 'error', listener: (err: Error) => void): this;
  addListener(event: 'close', listener: (code: number, reason: string) => void): this;
  addListener(event: 'upgrade', listener: (request: http.IncomingMessage) => void): this;
  addListener(event: 'open' , listener: () => void): this;
  addListener(event: 'ping' | 'pong', listener: (data: Buffer) => void): this;

  removeListener(event: 'close', listener: (code: number, reason: string) => void): this;
  removeListener(event: 'error', listener: (err: Error) => void): this;
  removeListener(event: 'upgrade', listener: (request: http.IncomingMessage) => void): this;
  removeListener(event: 'open' , listener: () => void): this;
  removeListener(event: 'ping' | 'pong', listener: (data: Buffer) => void): this;
}