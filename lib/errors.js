module.exports.TimeoutError = class TimeoutError extends Error {
  constructor() {
    super();
    this.name = 'TimeoutError';
  }
}

module.exports.InvalidRequestError = class InvalidRequestError extends Error {
  constructor(msg) {
    super('Invalid request: ' + JSON.stringify(msg, null, 2));
    this.name = 'InvalidRequestError';
  }
}